[![LogoBanner](https://raw.githubusercontent.com/SangeloDev/SangeloDev/main/Banner%20Dark%206818x2727.png)](https://sangelo.space)


## Hi, it's me! Sangelo.
I'm a guy who does stuff.

I enjoy creating a variety of things, including UI prototypes, scripts, and programs. Although I'm new to programming, I love learning new languages. I support open-source software and design my workflow around it, using Linux and self-hosted tools daily.<br>
For more info, you can contact me [here](mailto:contact@sangelo.space).
  
### Socials
[![Website](https://img.shields.io/badge/website-sangelo.space-00B1FA)](https://sangelo.space)
[![Discord](https://img.shields.io/badge/Discord-@sangelo-6E85D3)](https://discord.com/users/373525255102136341)
[![YouTube](https://img.shields.io/youtube/channel/subscribers/UC8fIfLHdrkWYjTEZUwMH-TQ?label=YouTube)](https://www.youtube.com/@sangeloo)
